const actions = {
  FETCHING: "FETCHING",
  FETCHING_SUCCESS: "FETCHING_SUCCESS",
  FETCHING_FAIL: "FETCHING_FAIL",
  FETCHING_NEW: "FETCHING_NEW",

  API_KEY: "da78e944e6184f658b10622a1be640d7",
  PAGE_SIZE: 5,

  //   'da78e944e6184f658b10622a1be640d7', // old
  // apiKey: 'd67803f9535e4d1998f04e78e42ae8dd', // new
};

export default actions;
