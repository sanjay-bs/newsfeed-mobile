import React, {Component, useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  SafeAreaView,
  KeyboardAvoidingView,
} from 'react-native';

import NetInfo, {useNetInfo} from '@react-native-community/netinfo';
import Animated, {Easing, EasingNode} from 'react-native-reanimated';
import AppContainer from './Navigation/Root';

import dataStore from './Redux/Store';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';

const App = () => {
  const netInfo = useNetInfo();
  let count = 0;

  const height = new Animated.Value(0);

  useEffect(() => {
    netInfo.isConnected
      ? Animated.timing(height, {
          toValue: 0,
          delay: 2000,
          easing: EasingNode.linear,
          duration: 300,
          useNativeDriver: true,
        }).start()
      : Animated.timing(height, {
          toValue: 30,
          //   delay: 1000,
          duration: 400,
          easing: EasingNode.bounce,
          useNativeDriver: true,
        }).start();
  }, [netInfo]);

  //   useEffect(() => {}, []);

  return (
    <Provider store={dataStore.store}>
      <PersistGate loading={null} persistor={dataStore.persistor}>
        <SafeAreaView style={[styles.mainView]}>
          <Animated.View
            style={[
              styles.internetView,
              {
                backgroundColor: netInfo.isConnected ? 'green' : 'red',
                height: height,
                // height: netInfo.isConnected ? 0 : 30,
                // transform: [{translateY: height}],
              },
            ]}>
            <Text style={{color: '#FFFFFF', fontWeight: 'bold'}}>
              {netInfo.isConnected ? 'ONLINE' : 'OFFLINE'}
            </Text>
          </Animated.View>
          <AppContainer />
        </SafeAreaView>
      </PersistGate>
    </Provider>
  );
};

export default App;

const styles = StyleSheet.create({
  mainView: {flex: 1, backgroundColor: '#FFFFFF'},
  internetView: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
