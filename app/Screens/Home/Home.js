import axios from "axios";
import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  FlatList,
  ScrollView,
  TextInput,
  RefreshControl,
  Image,
  Modal,
  ImageBackground,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Dimensions,
  Keyboard,
} from "react-native";
import NetInfo, { useNetInfo } from "@react-native-community/netinfo";

import { useSelector, useDispatch } from "react-redux";
import NewsAction from "./../../Redux/Actions/NewsAction";
import DateRangePicker from "react-native-daterange-picker";
import moment from "moment";

const { height, width } = Dimensions.get("screen");

const Home = ({ navigation, ...props }) => {
  const dispatch = useDispatch();
  const newsApiData = useSelector((reducer) => {
    return reducer;
  });

  const [page, setPage] = useState(1);
  const [query, setQuery] = useState("");
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [displayedDate, setDisplayedDate] = useState(moment());
  const [datePicker, setdatePicker] = useState(false);

  const fetchNews = () => {
    NetInfo.fetch()
      .then((netWork) => {
        if (newsApiData.newsData.length > 0) {
          setPage(newsApiData.currentPage);
          setStartDate(newsApiData.from);
          setEndDate(newsApiData.to);
          setQuery(newsApiData.q);
        }
        if (netWork.isConnected) {
          if (query) {
            dispatch(
              NewsAction({
                page: page,
                q: newsApiData.q ? newsApiData.q : query,
                ...(startDate && { from: startDate }),
                ...(endDate && { to: endDate }),
              })
            );
          } else {
          }
        } else {
          // if (newsApiData.newsData.length > 0) {
          //   setPage(newsApiData.currentPage);
          //   setStartDate(newsApiData.from);
          //   setEndDate(newsApiData.to);
          //   setQuery(newsApiData.q);
          // }
          Alert.alert("", "No Internet Connection");
        }
      })
      .catch((er) => console.log("Error", er));
  };
  useEffect(() => {
    fetchNews();
  }, []);

  const loadMore = (item) => {
    if (newsApiData.totalItems / 5 == page) {
      return;
    }
    NetInfo.fetch()
      .then((netWork) => {
        if (netWork.isConnected) {
          if (!newsApiData.loading) {
            dispatch(
              NewsAction({
                page: newsApiData.currentPage + 1,
                q: query,
                ...(startDate && { from: startDate }),
                ...(endDate && { to: endDate }),
              })
            );
          } else {
            console.log("NO Load More");
            return;
          }
        }
      })
      .catch((e) => {
        Alert.alert("", "Something went wrong");
      });
  };

  const searchText = () => {
    // console.warn(query);
    Keyboard.dismiss();
    NetInfo.fetch()
      .then((netWork) => {
        if (netWork.isConnected) {
          dispatch(
            NewsAction({
              page: 1,
              q: query,
              ...(startDate && { from: startDate }),
              ...(endDate && { to: endDate }),
            })
          );
        } else {
          // if (newsApiData.newsData.length > 0) {
          //   setPage(newsApiData.currentPage);
          //   setStartDate(newsApiData.from);
          //   setEndDate(newsApiData.to);
          // }
          Alert.alert("", "No Internet Connection");
        }
      })
      .catch((er) => console.log("Error", er));
  };

  const removeDuplicates = (itemList) => {
    let jsonObject = itemList.map(JSON.stringify);
    let uniqueSet = new Set(jsonObject);
    let uniqueArray = Array.from(uniqueSet).map(JSON.parse);
    return uniqueArray;
  };

  const navigateWebView = () =>
    navigation.navigate("WebViewScreen", { uri: item.url });

  const dataPickerChange = (value) => {
    if (value.displayedDate) {
      setDisplayedDate(value.displayedDate);
    }
    if (value.startDate) {
      setStartDate(value.startDate);
      setEndDate(null);
      console.log(moment(value.startDate).format("YYYY-MM-DD"));
      NetInfo.fetch()
        .then((netWork) => {
          if (netWork.isConnected) {
            dispatch(
              NewsAction({
                q: query,
                ...(endDate && { to: endDate }),
                page: 1,
                from: "" + moment(value.startDate).format("YYYY-MM-DD") + "",
              })
            );
          }
        })
        .catch((err) => {
          console.log("Err", err);
        });
    }
    if (value.endDate) {
      setEndDate(value.endDate);
      setdatePicker(false);
      NetInfo.fetch()
        .then((netWork) => {
          if (netWork.isConnected) {
            dispatch(
              NewsAction({
                page: 1,
                q: query,
                from: "" + moment(startDate).format("YYYY-MM-DD") + "",
                to: "" + moment(value.endDate).format("YYYY-MM-DD") + "",
              })
            );
          }
        })
        .catch((err) => {
          console.log("Err", err);
        });
    }
  };

  ListFooterCo = () => {
    return newsApiData.loading && newsApiData.loading ? (
      <View style={{ padding: 10 }}>
        <ActivityIndicator color={"#004F4F"} />
      </View>
    ) : (
      <View />
    );
  };
  renerIt = ({ item, index }) => {
    return (
      <View style={{ paddingVertical: 10, paddingHorizontal: 15 }}>
        <TouchableOpacity activeOpacity={0.8} onPress={navigateWebView}>
          <View
            style={{
              borderRadius: 10,
              backgroundColor: "#FFF",
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,

              elevation: 5,
            }}
          >
            <View
              style={{
                overflow: "hidden",
                borderRadius: 10,
              }}
            >
              {item.urlToImage ? (
                <Image
                  style={{ height: 150 }}
                  source={{ uri: item.urlToImage }}
                />
              ) : (
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    padding: 20,
                  }}
                >
                  <Text
                    style={{
                      color: "grey",
                      fontSize: 18,
                      textAlign: "center",
                    }}
                  >
                    Image Unavailable
                  </Text>
                </View>
              )}
            </View>
            {item.title ? (
              <View
                style={{
                  padding: 10,
                }}
              >
                <Text
                  style={{
                    fontSize: 16,
                  }}
                >
                  {item.title}
                </Text>
              </View>
            ) : null}
            {moment(item.publishedAt).isValid() && (
              <View style={{ paddingHorizontal: 10 }}>
                <Text>
                  Published at :
                  {moment(item.publishedAt).format(" DD-MM-YYYY hh:mm A")}
                </Text>
              </View>
            )}

            <View
              style={{
                padding: 10,
              }}
            >
              <Text>
                Author:{" "}
                {item.author ? (
                  item.author
                ) : (
                  <Text style={{ color: "grey" }}>Not available</Text>
                )}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  emptList = () => (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        padding: 40,
      }}
    >
      <Text>NO NEWS AVAILABLE</Text>
    </View>
  );
  extractKey = (item) => item.url;

  return (
    <View style={[styles.container]}>
      <View>
        <View style={{ borderWidth: 0 }}>
          {!startDate && !endDate ? (
            <Text
              style={{ textAlign: "center" }}
              onPress={() => setdatePicker(true)}
            >
              NO DATE RANGE SELECTED
            </Text>
          ) : (
            <View
              style={{
                flexDirection: "row",
                padding: 10,
                justifyContent: "space-evenly",
              }}
            >
              <View style={styles.datesContainer}>
                <Text style={[styles.dateText]}>From Date</Text>
                <Text style={[styles.dateText]}>
                  {moment(startDate).format("DD-MMM-YYYY")}
                </Text>
              </View>
              <View style={{ height: 10, width: 10 }} />
              <View style={[styles.datesContainer]}>
                <Text style={[styles.dateText]}>To Date</Text>
                <Text style={[styles.dateText]}>
                  {endDate
                    ? moment(endDate).format("DD-MMM-YYYY")
                    : "NOT SELECTED"}
                </Text>
              </View>
            </View>
          )}
        </View>
        <View
          style={{ alignItems: "center", justifyContent: "center", padding: 5 }}
        >
          <TouchableOpacity
            style={[styles.pickerBtnTouch]}
            onPress={() => setdatePicker(true)}
          >
            <Text style={{ color: "#FFFFFF" }}>CHANGE DATE SLOT</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={[styles.searchView]}>
        <TextInput
          style={[styles.searchTextInput]}
          returnKeyType={"search"}
          placeholder={"Enter Search Text"}
          placeholderTextColor={"grey"}
          onChangeText={(val) => setQuery(val)}
          value={query}
          onSubmitEditing={() => {
            searchText();
          }}
        />
        <TouchableOpacity
          onPress={() => {
            searchText();
          }}
          style={[styles.searchBtn]}
        >
          <Image
            style={{ height: 20, width: 20 }}
            source={require("../../Assets/Search.png")}
          />
        </TouchableOpacity>
      </View>
      <View style={{ flex: 1 }}>
        <FlatList
          keyExtractor={extractKey}
          showsVerticalScrollIndicator={false}
          // ListHeaderComponent={() => {

          //   // return (
          //   //   <View style={{padding: 15, backgroundColor: '#ffdf87'}}>
          //   //     <Text style={{fontSize: 18, fontWeight: 'bold'}}>
          //   //       News Feed
          //   //     </Text>
          //   //   </View>
          //   // );
          // }}
          // stickyHeaderIndices={[0]}
          data={removeDuplicates(newsApiData.newsData)}
          ListEmptyComponent={emptList}
          onEndReached={loadMore}
          onEndReachedThreshold={0.4}
          renderItem={renerIt}
          ListFooterComponent={ListFooterCo}
        />
      </View>
      <Modal
        visible={datePicker}
        onRequestClose={() => setdatePicker(false)}
        animationType={"slide"}
        transparent={true}
      >
        <View
          style={{
            borderWidth: 1,
            height: height,
            width: width,
            backgroundColor: "rgba(0, 0, 0, 0.3)",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <DateRangePicker
            backdropStyle={styles.dateRangeBackdrop}
            onChange={dataPickerChange}
            open={datePicker}
            endDate={endDate}
            startDate={startDate}
            displayedDate={displayedDate}
            minDate={moment().subtract(28, "d")}
            maxDate={moment()}
            range
          >
            <View />
          </DateRangePicker>
          <View style={[styles.closeButtonView]}>
            <TouchableOpacity
              style={[styles.closeButtonTouch]}
              onPress={() => {
                setdatePicker(false);
              }}
            >
              <Text
                style={{
                  fontSize: 20,
                  color: "red",
                }}
              >
                Close
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};
export default Home;

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: "#FFFFFF" },
  datesContainer: { flex: 1, borderWidth: 1, alignItems: "center" },
  dateText: {
    fontSize: 14,
    fontWeight: "bold",
  },
  dateRangeBackdrop: {
    zIndex: -1,
    backgroundColor: "transparent",
  },
  closeButtonView: {
    position: "absolute",
    right: 0,
    alignItems: "flex-end",
    paddingHorizontal: 20,
    top: 50,
    zIndex: 3,
  },
  closeButtonTouch: {
    zIndex: 3,
    padding: 10,
    borderRadius: 20,
    backgroundColor: "#FFFFFF",
    // borderWidth: 2,
  },
  searchBtn: {
    borderRadius: 90,
    padding: 10,
    marginVertical: 7,
    marginHorizontal: 5,
    backgroundColor: "#4589ff",
  },
  searchTextInput: {
    color: "#000000",
    flex: 1,
    marginHorizontal: 5,
  },
  searchView: {
    borderWidth: 1,
    paddingHorizontal: 10,
    borderRadius: 30,
    margin: 10,
    backgroundColor: "#ffdf87",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  pickerBtnTouch: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    // borderWidth: 1,
    borderRadius: 20,
    backgroundColor: "#4589ff",
  },
});

{
  /*  <View>
        <FlatList
          contentContainerStyle={{padding: 15}}
          horizontal
          showsHorizontalScrollIndicator={false}
          data={[
            {
              id: 1,
              title: 'Entertainment',
              img: require('../../Assets/entertainment.jpeg'),
            },
            {
              id: 2,
              title: 'Sports',
              img: require('../../Assets/sports.jpeg'),
            },
            {
              id: 3,
              title: 'Business',
              img: require('../../Assets/buissness.jpeg'),
            },
          ]}
          ItemSeparatorComponent={() => {
            return <View style={{height: 10, width: 10}} />;
          }}
          renderItem={({item, index}) => {
            return (
              <ImageBackground
                style={{
                  width: 180,
                  height: 100,
                  borderRadius: 15,
                  overflow: 'hidden',
                }}
                source={item.img}>
                <View
                  style={{
                    width: 180,
                    height: 100,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: 'rgba(0,0,0,0.6)',
                  }}>
                  <Text style={{color: '#FFFFFF', fontWeight: 'bold'}}>
                    {item.title}
                  </Text>
                </View>
              </ImageBackground>
            );
          }}
        /> 
      </View>*/
}
