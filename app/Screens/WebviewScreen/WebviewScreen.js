import React, {Component} from 'react';
import {View, StyleSheet, AppRegistry} from 'react-native';
import {WebView} from 'react-native-webview';
const WebViewScreen = props => {
  return (
    <View style={styles.container}>
      <WebView source={{uri: props.route.params.uri}} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default WebViewScreen;
