import React, {Component} from 'react';

import {View, Text, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../Screens/Home/Home';
import WebViewScreen from '../Screens/WebviewScreen/WebviewScreen';

const RootStacK = createStackNavigator();

const Root = () => {
  return (
    <RootStacK.Navigator>
      <RootStacK.Screen
        name={'Home'}
        options={{
          headerTitle: 'News Feed',
          headerTitleAlign: 'center',
        }}
        component={Home}
      />
      <RootStacK.Screen
        name={'WebViewScreen'}
        options={{headerTitle: 'NEWS', headerTitleAlign: 'center'}}
        component={WebViewScreen}
      />
    </RootStacK.Navigator>
  );
};

const AppContainer = () => {
  return (
    <NavigationContainer>
      <Root />
    </NavigationContainer>
  );
};

export default AppContainer;
