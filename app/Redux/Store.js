import { createStore, applyMiddleware } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import AsyncStorage from "@react-native-async-storage/async-storage";
import NewsReducer from "./Reducer/NewsReducer";
import thunk from "redux-thunk";
import logger from "redux-logger";

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  // whitelist: ['NewsReducer'],
};

const persistedReducer = persistReducer(persistConfig, NewsReducer);

let store = createStore(persistedReducer, applyMiddleware(thunk, logger));

let persistor = persistStore(store);

export default { store, persistor };
