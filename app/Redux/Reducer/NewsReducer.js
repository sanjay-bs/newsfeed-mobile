import actions from "../../Config/Constants";

actions;
const initialState = {
  loading: false,
  actionLogs: null,
  loading: false,
  error: false,
  totalItems: 0,
  totalPages: 1,
  currentPage: 1,
  newsData: [],
  from: null,
  to: null,
  errorTimes: 0,
  q: "",
};

const NewsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.FETCHING:
      return { ...state, error: false, loading: true };
    case actions.FETCHING_SUCCESS:
      console.log("SUCCESS ACTION PAYLOAD", action.payload);
      const responseData = action.payload.data;

      const totalPages = Math.ceil(responseData.totalResults / 5);
      const currentpage = action.payload.page;
      const from = action.payload.from;
      const to = action.payload.to;
      const q = action.payload.q ? action.payload.q : "";

      return {
        ...state,
        actionLogs: action.payload,
        error: false,
        loading: false,
        totalItems: responseData.totalResults,
        totalPages: totalPages,
        currentPage: currentpage,
        from: from,
        q: q,
        to: to,
        newsData:
          currentpage == 1
            ? [...responseData.articles]
            : [...state.newsData, ...responseData.articles],
      };
    case actions.FETCHING_FAIL:
      return {
        ...state,
        errorTimes: state.errorTimes + 1,
        error: true,
        loading: false,
      };
    default:
      return state;
  }
};

export default NewsReducer;
