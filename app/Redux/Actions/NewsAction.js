import axios from "axios";
import actions from "../../Config/Constants";

const NewsAction = (params) => {
  return (dispatch) => {
    try {
      dispatch({ type: actions.FETCHING, payload: null });

      const uri = "https://newsapi.org/v2/everything";
      //   const uri =
      // 'https://newsapi.org/v2/everything?apiKey=da78e944e6184f658b10622a1be640d7&q=""';
      const options = {
        headers: {
          Authorization: actions.API_KEY,
        },
        params: {
          // apiKey: actions.API_KEY, // old
          ...params,
          pageSize: actions.PAGE_SIZE,
          q: params.q ? params.q : '""',
          // category: 'entertainment',
          // sortBy: 'publishedAt',
        },
      };

      axios
        .get(uri, options)
        .then((res) => {
          console.log(res);
          if (res.status == 200 && res.data.status == "ok")
            dispatch({
              type: actions.FETCHING_SUCCESS,
              payload: { ...res, ...params },
            });
          else {
            dispatch({ type: actions.FETCHING_FAIL, payload: { ...res } });
          }
        })
        .catch((e) => {
          // console.log('e', e);
          // console.log('e.config', e.config);
          // console.log('e.request', e.request);
          // console.log('e.response', e.response);
          // console.log('e.message', e.message);

          if (e.response) {
            dispatch({ type: actions.FETCHING_FAIL, payload: e.response });
          } else if (e.request) {
            console.log(e.request);
            dispatch({ type: actions.FETCHING_FAIL, payload: e.request });
          } else {
            console.log("Error", e.message);
            dispatch({ type: actions.FETCHING_FAIL, payload: e.message });
          }
        });
    } catch (error) {
      console.log(error);
      dispatch({ type: actions.FETCHING_FAIL, payload: null });
    }
  };
};

export default NewsAction;
